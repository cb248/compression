
public class Compress {

	public static void main(String[] args) {
		//some hardcoded tests - used when user has not provided arguments
		//String[] test = {"a", "0.3", "b", "0.2", "c", "0.2", "d" , "0.2", "e", "0.1", "eof", "e", "dbdcbaacabcabadaaadbbde"};
		String[] test1 = {"a", "0.4", "b", "0.5", "c", "0.1", "eof", "c", "abbbc"};
		
		String[] arguments;
		//find out if an information source has been provided by the user 
		if (args.length == 0){
			arguments = test1;
		}
		else {
			arguments = args;
		}
		SequenceStats s = new SequenceStats();
		s.initialiseInfoSource(arguments);
	}
}
