import java.io.IOException;
import java.io.StringReader;


public class Decode extends Coder{

	int buf;
	String message;
	Symbol[] source_info;
	
	public Decode(String message, Symbol[] source_info, int total_count){
		this.message = message;
		this.source_info = source_info;
		this.total_count = total_count;
		System.out.println("\nDecoding " + message);
		//pass the first B bits to the buffer
		this.buf = Integer.parseInt((message.substring(0,B)), 2);
		printRange();
		setInitialRange(source_info);
	}
	
	public void decode(){
		String output = "";
		Symbol chosen_symbol;
				StringReader read = setupReader(message);
		
		while (true){
			chosen_symbol = updateSymbolRanges(source_info, buf);
			output += chosen_symbol.getValue();
			System.out.println("\n\tout: " + output);
			printRange();
			updateRange(chosen_symbol);
			if (chosen_symbol.isEof){
				break;
			}
			while(true){	
				
				if((low & MSB_MASK) == (high & MSB_MASK)){
					System.out.println("RESCALE " + chosen_symbol.getValue());
					System.out.println("\tout: " + output);
					printRange();
					//shift left and add the next bit from the source
					buf = ((buf << 1) & MASK) | readBit(read);
					high = ((high << 1) & MASK) | 1;
					low = (low << 1) & MASK;

				} else if ((low & ~high & MSB2_MASK) != 0){
					buf = (buf & MSB_MASK) | ((buf << 1) & (MASK >>> 1)) | readBit(read);
					high = (high ^ MSB2_MASK);
					high = ((high << 1) & MASK) | 1;
					low = (low ^ MSB2_MASK);
					low = ((low << 1) & MASK) | 0;
				} else {
					break;
				}
			}

		}
		
		System.out.println("decoding is " + output);
	}
	
	//update the ranges for each symbol depending on the overall high and low
	Symbol updateSymbolRanges(Symbol[] source_info, int message){
		Symbol chosen_symbol = null;
		System.out.print("choose");
		for (int i = 1; i < source_info.length; i++){
			int l = calculateLow(source_info, i);
			int h = calculateHigh(source_info, i);
			source_info[i].setLow(l);
			source_info[i].setHigh(h);
			System.out.print(" [" + Integer.toBinaryString(l) + "/" + l + " , " + Integer.toBinaryString(h) + "/" + h + "]");
			if ((l <= message) && (h >= message)){
				chosen_symbol = source_info[i];
				System.out.print("!");
			}
		}

		return chosen_symbol;
	}
	
	void printRange(){
		System.out.println("\tbuf=" + Integer.toBinaryString(buf) + " [l/u] = [" + Integer.toBinaryString(low) + "/" + low + " , " + Integer.toBinaryString(high) + "/" + high + "]" );
	}
	
	//iterate the string reader through the first B bits (not needed)
	public StringReader setupReader(String message){
		StringReader read = new StringReader(message);
		for (int i = 0; i < B; i++){
			try {
				read.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return read;
	}
	
	int readBit(StringReader read){
		try {
			int r = read.read();
			if(r == -1){
				return 0; //pad with zeros
			}
			char bit = (char)r;
			if (bit == '1'){
				return 1;
			}
			else return 0;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	
}
