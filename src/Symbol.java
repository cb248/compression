
public class Symbol {

	//information about each symbol
	String value;
	double prob = 0.0;
	int cum_freq = 0;
	boolean isEof = false;
	int low = 0;
	int high = 0;
	
	
	public Symbol(String value, String prob){
		this.value = value;
		this.prob = Double.parseDouble(prob);
		
	}
	
	public String getValue(){
		return this.value;
	}
	
	public void setValue(String v){
		this.value = v;
	}
	
	public double getProb(){
		return this.prob;
	}
	
	public void setProb(double p){
		this.prob = p;
	}
	
	public boolean getEof(){
		return this.isEof;
	}
	
	public void setAsEof(){
		this.isEof = true;
	}
	
	public int getCumFreq(){
		return this.cum_freq;
	}
	
	public void setCumFreq(int c){
		this.cum_freq = c;
	}
	
	public int getLow(){
		return this.low;
	}
	
	public void setLow(int low){
		this.low = low;
	}
	
	public int getHigh(){
		return this.high;
	}
	
	public void setHigh(int high){
		this.high = high;
	}
	
	
	
	
	
	
	
}
