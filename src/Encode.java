
public class Encode extends Coder{

	int scale3 = 0;
	Symbol the_symbol;
	String tag = "";
	Symbol[] source_info;
	String sequence;
	
	public Encode(String sequence, Symbol[] source_info, int total_count){
		System.out.println("\nBuffer size: " + B);
		this.sequence = sequence;
		this.source_info = source_info;
		this.total_count = total_count;
		System.out.println("encoding " + sequence.toString());
		setInitialRange(source_info);
		printRange();
	}
	
	public String encode(){
		Symbol s = source_info[0];
		String output = "";
		for (int i = 0; i < sequence.length(); i ++){
			//identify symbol to encode
			for(int j = 0; j < source_info.length; j ++){
				if(source_info[j].getValue().contains(sequence.substring(i, i+1))){
					s = source_info[j];
					break;
				}
			}
			
			updateSymbolRanges(source_info, s);
			updateRange(s);
			printRange();
			while(true){
				//if the most-significant-bits for both high and low are the same
				if((low & MSB_MASK) == (high & MSB_MASK)){
					System.out.println("RESCALE " + s.getValue());
					printRange();
					//add the most-significant-bit to the tag
					int bit = (low >>> (B - 1));
					tag += bit;
					//shift both high and low left by one bit
					//add a bit to the lsb of high
					high = ((high << 1) & MASK) | 1;
					//a zero will be added to lsb of low (without explicitly specifying it)
					low = (low << 1) & MASK;
					//if the Scale3 condition holds, perform operation on bits
					while(scale3 > 0) {
						//complement the bit (so obtain opposite bit)
						tag += 1 - bit;
						scale3--;
					}	
				} else 
					//when the second-most-significant-bits are different 
					if ((low & ~high & MSB2_MASK) != 0){
					//complement the second-most-significant bit in high
					high = (high ^ MSB2_MASK);
					//shift to the left and add one bit to the lsb
					high = ((high << 1) & MASK) | 1;
					low = (low ^ MSB2_MASK);
					low = ((low << 1) & MASK) | 0;
					//the Scale3 condition is invoked
					scale3++;
				} else {
					break;
				}
			}
			//check for the end of the sequence
			if (s.isEof){
				output = finishTag(output);
				System.out.println("output is " + output);
			}
		}
		return output;
	}

	//print information about the overall boundry 
	void printRange(){
		System.out.println("\ttag=" + (tag.toString()));
		System.out.println("\ts=" + scale3 + " [l/u] = [" + Integer.toBinaryString(low) + "/" + low + " , " + Integer.toBinaryString(high) + "/" + high + "]" );

	}

	//generate the new lower and upper bounds for each symbol
	void updateSymbolRanges(Symbol[] source_info, Symbol s){
		System.out.print("SCAN " + s.getValue() +" choose");
		for (int i = 1; i < source_info.length; i++){
			int l = calculateLow(source_info, i);
			int h = calculateHigh(source_info, i);	
			source_info[i].setLow(l);
			source_info[i].setHigh(h);
			System.out.print(" [" + Integer.toBinaryString(l) + "/" + l + " , " + Integer.toBinaryString(h) + "/" + h + "]");
			if (source_info[i].equals(s)){
				System.out.print("!");
			}
		}
		System.out.println("");
	}
	
	//append the low value to the end of the tag to finish it
	String finishTag(String output){
		String pad = "%" + B + "s";
		output = String.format(pad, Integer.toBinaryString(low));
		output = output.replace(" ", "0");
		output = tag + output;
		return output;
	}
	
}