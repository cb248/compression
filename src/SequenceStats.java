import java.util.ArrayList;
import java.util.Random;


public class SequenceStats {
	
	static Symbol[] source_info;

	static int total_count = 0;
	static String eof = "";

	public void initialiseInfoSource(String[] args){
		setSymbols(args);
		double lowest = lowestProb(source_info);
		total_count = setTotal(lowest);
		setCumulativeFrequency(source_info);
		printInformationSource(source_info);
		
		String the_sequence = getSequence(args);
		
		//perform encoding on the provided sequence of symbols
		Encode e = new Encode(the_sequence, source_info, total_count);
		String message = e.encode();
		printDivide();
		//decode the message optained by the encoded sequence
		Decode d = new Decode(message, source_info, total_count);
		d.decode();
		
		
		
	}
	
	//if a sequence has been provided by the user, use it - otherwise generate one
	String getSequence(String[] args){
		if ((args.length % 2) == 0){
			return generateSequence(source_info);
		}
		else {
			return args[args.length - 1];
		}
		
	}
	
	//create an alphabet of symbols from the given information source
	public void setSymbols(String[] args){
		ArrayList<Symbol> symbols = new ArrayList<Symbol>();
		symbols.add(new Symbol("0", "0"));
		for(int i = 0; i < args.length - 1; i++){
			int temp = i+1;
			symbols.add(new Symbol(args[i], args[temp]));
			if(args[temp+1].contains("eof")){
				eof = args[temp+2];
				break;
			}
			i++;
		}
		convertToArray(symbols);
		setEOF(eof);
	}

	static void convertToArray(ArrayList<Symbol> a){
		source_info = new Symbol[a.size()];
		source_info = a.toArray(source_info);
	}

	//find the corresponding end of file symbol to that provided by the user
	static void setEOF(String eof_value){
		for(int i = 0; i < source_info.length; i ++){
			if(source_info[i].getValue().contains(eof_value)){
				source_info[i].setAsEof();
				break;
			}
		}
	}

	//attach a cumulative frequency to each symbol
	void setCumulativeFrequency(Symbol[] source_info){
		//set first frequency to zero
		source_info[0].setCumFreq(0);
		for(int i = 1; i < source_info.length; i++){
			source_info[i].setCumFreq((int) (source_info[i].getProb() * total_count) + source_info[i-1].getCumFreq());
		}
	}

	//find the lowest probability in the alphabet - used to calculate total_count
	public double lowestProb(Symbol[] source_info){
		double lowest_prob = 1.0;
		for(int i = 1; i < source_info.length; i++){
			if(source_info[i].getProb() < lowest_prob){
				lowest_prob = source_info[i].getProb();
			}
		}
		return lowest_prob;
	}

	//find total_count for integer operations
	int setTotal(double lowest_prob){
		int total = (int) (1 / lowest_prob);
		return total;
	}

	
	static void printInformationSource(Symbol[] source_info){
		System.out.println("Information source: ");
		for (int i = 1; i < source_info.length; i++){
			System.out.println("P(" + source_info[i].getValue() + ")=" + source_info[i].getProb());
		}
		System.out.println("Eof is: " + eof);
		System.out.println("Total Count: " + total_count);
		System.out.print("Cumulative Frequency: ");
		for (int i = 0; i < source_info.length; i++){
			System.out.print("[" + source_info[i].getCumFreq() + "] ");
		}
	}

	//create a random sequence to be encoded using probabilities of alphabet
	public String generateSequence(Symbol[] source_info){
		String sequence = "";
		Random rand = new Random();
		while (true){
			int num = rand.nextInt(total_count);
			for (int i = 1; i < source_info.length; i ++){
				if ((source_info[i-1].getCumFreq() <= num) && (source_info[i].getCumFreq()>num)){
					sequence += source_info[i].getValue();
					if (source_info[i].isEof){
						System.out.println(sequence);
						return sequence;
					}
				}
			}
		}
	}
	
	//ensure that the probabilities given sum up to one
	Boolean checkProbabilities(Symbol[] source_info){
		int total = 0;
		for (int i = 0; i < source_info.length - 3; i++){
			total += source_info[i].getProb();
		}
		if (total != 1){
			System.out.println("The sum of the probabilities for the given alphabet must equal one.");
			System.exit(-1);
		}
		return true;
	}
	
	void printDivide(){
		System.out.println("***********************************************");

	}

}