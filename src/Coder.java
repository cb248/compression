
public abstract class Coder {
	
	public static final int B  = 6;  //buffer value
	public static final int MSB_MASK = 1 << (B - 1);
	public static final int MSB2_MASK = 1 << (B - 2);
	public static final int MASK = (1 << B) - 1;
	int high;
	int low;
	int total_count;
	
	//update the "overall" low and high bounds
	public void updateRange(Symbol s){
		low = s.getLow();
		high = s.getHigh();
	}
	
	//initialise the overall bound to 0000... (up to B) and 1111... (up to B)
	void setInitialRange(Symbol[] source_info){
		low = (int) ((0L << (B - 0)) - 0);
		high = (int) ((1L << (B - 0)) - 1);
		source_info[0].setLow(low);
		source_info[0].setHigh(high);
	}
	
	int calculateLow(Symbol[] source_info, int i){
		return low + ((high -  low + 1) * source_info[i-1].getCumFreq() / total_count);
	
	}
	
	int calculateHigh(Symbol[] source_info, int i){
		return low + ((high - low + 1) * source_info[i].getCumFreq() / total_count) - 1;	
	}
	
	abstract void printRange();
	
}
